package pokemon.atelier2.MS_controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import pokemon.atelier2.MS_model.Card;
import pokemon.atelier2.MS_model.User;
import pokemon.atelier2.MS_model.UserCards;

@Service
public class AuthService {
	
	@Autowired
	UserService userService;
	
	@Autowired
	CardService cardService;
	
	public Object register(User u) {
		Object result = -1;
		if(!StringUtils.isEmpty(u.getLogin()) && !StringUtils.isEmpty(u.getPassword()) && !StringUtils.isEmpty(u.getName())){
			boolean isTaken = userService.isLoginTaken(u.getLogin());
	        
			if(!isTaken){
				
				userService.addUser(u);

				User newUser = userService.getUserByLogin(u.getLogin());
				
				List<Card> cardsList = cardService.getCardsByUserId(newUser.getId());
				
				return new UserCards(newUser, cardsList);
			}
		}
		return result;
	}
	
	public Object login(String login, String pwd) {
		Object result = -1;
		if(!login.isEmpty() && !pwd.isEmpty()){
			
			User user = userService.getUserByLogin(login);

			if(user != null) {
				if(user.getPassword().equals(pwd)) { 
					List<Card> cardList = cardService.getCardsByUserId(user.getId());

					return new UserCards(user,cardList);
				}
			}
		}
		return result;
	}
}
