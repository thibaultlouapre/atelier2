package pokemon.atelier2.MS_controller;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import pokemon.atelier2.MS_api.API;
import pokemon.atelier2.MS_model.CardDescription;
import pokemon.atelier2.MS_repository.CardDescriptionRepo;

@Service
public class CardDescriptionService
{
	@Autowired
	private CardDescriptionRepo cardDescriptionRepo;
	
	public CardDescriptionService() {
	}
	
	public List<CardDescription> getAllCardsDes() {
		return cardDescriptionRepo.findAll();
	}
	
	public CardDescription getRandomCardDes() {
		CardDescription cd = API.getInstance().getRandomCard();
		addCard(cd);
		return cd;
	}
	
	public CardDescription getCardDesById(int id) {
		Optional<CardDescription> cardDesOpt = cardDescriptionRepo.findById(id);
		return cardDesOpt.isPresent() ? cardDesOpt.get() : null;
	}

	public void addCard(CardDescription cardDes) {
		cardDescriptionRepo.save(cardDes);
	}
	
	public void deleteCardDesById(int id) {
		cardDescriptionRepo.delete(this.getCardDesById(id));
	}
}