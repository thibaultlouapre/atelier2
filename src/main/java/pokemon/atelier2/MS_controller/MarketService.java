package pokemon.atelier2.MS_controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pokemon.atelier2.MS_model.Card;
import pokemon.atelier2.MS_model.User;
import pokemon.atelier2.MS_model.UserCards;

@Service
public class MarketService {
	
	@Autowired
	UserService userService;
	
	@Autowired
	CardService cardService;
	
	static final int REMOVED_FROM_SALE = -2;
	static final int ERROR = -1;

	public int buyCard(int idBuyer, int idCard) {
		int isTransacOk = -1;
	    
	    UserCards buyerC = userService.getUser(idBuyer);
	    User buyer = buyerC.getUser();
	    Card boughtC = cardService.getCardById(idCard);

		if(boughtC != null && buyer != null) {
			int price = boughtC.getCardDescription().getPrice();
			if(buyer.getBalance() >= price && boughtC.isForSale()) {
				int sellerid = boughtC.getOwnerId();

			    UserCards sellerC = userService.getUser(sellerid);
			    User seller = sellerC.getUser();
			    
			    userService.updateBalance(seller.getId(), seller.getBalance() + price);
			    userService.updateBalance(buyer.getId(), buyer.getBalance() - price);
			    
			    cardService.setForSale(idCard, false);
			    
			    cardService.updateOwner(boughtC.getId(), idBuyer);
			    
				isTransacOk = buyer.getBalance()-price;
			}
		}
		return isTransacOk;
	}
	
	public int sellCard(int idSeller, int idCard) {
		int sellStatus = ERROR;
	    UserCards sellerC = userService.getUser(idSeller);
	    User seller = sellerC.getUser();

		Card sellingCard = cardService.getCardById(idCard);
		int ownerId = sellingCard.getOwnerId();

	    UserCards ownerC = userService.getUser(ownerId);
	    User owner = ownerC.getUser();
		
		if(seller.getId() == owner.getId()) {
			cardService.setForSale(idCard, !sellingCard.isForSale());
			sellStatus = sellingCard.isForSale() ? REMOVED_FROM_SALE : seller.getBalance();
		}
		return sellStatus;
	}

	public List<Card> getMarketCards() {
        List<Card> cards = cardService.getMarketCardsList();
		return cards;
	}
	
	
}