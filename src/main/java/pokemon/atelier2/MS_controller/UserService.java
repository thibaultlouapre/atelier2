package pokemon.atelier2.MS_controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pokemon.atelier2.MS_model.Card;
import pokemon.atelier2.MS_model.CardDescription;
import pokemon.atelier2.MS_model.User;
import pokemon.atelier2.MS_model.UserCards;
import pokemon.atelier2.MS_repository.UserRepository;


@Service
public class UserService {
	
	@Autowired
	CardService cardService;
	
	@Autowired
	CardDescriptionService cardDescriptionService;

	@Autowired
	private UserRepository userRepository;
	
	public UserService() {
	}
	
	public void addUser(User user) {
		for(int i = 0; i < 5; i++) {
	        CardDescription cd = cardDescriptionService.getRandomCardDes();
			user.setBalance(200);
			userRepository.save(user);
	        Card c = new Card(cd, getUserByLogin(user.getLogin()).getId(), false);
			cardService.addCard(c);
		}
	}
	
	public UserCards getUser(int id) {
		Optional<User> uOpt = userRepository.findById(id);
		if(uOpt.isPresent()) {
			User u = uOpt.get();
			List<Card> cards = null; //getCardsByUserId(u.getId());
			UserCards usercards = new UserCards(u,cards);
			return usercards;
		}
		return null;
	}
	
	public List<User> getUsers() {
		List<User> userList = (List<User>) userRepository.findAll();
		return userList;
	}
	
	public boolean isLoginTaken(String login) {
		return getUserByLogin(login) != null;
	}
	
	public User getUserByLogin(String login) {
		List<User> users = getUsers();
		User userByLogin = null;
		for (User user : users) {
			if (user.getLogin().equals(login)) {
				userByLogin = user;
			}
		}
		return userByLogin;
	}
	
	public void updateBalance(int id, int newBalance) {
		Optional<User> uOpt = userRepository.findById(id);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			u.setBalance(newBalance);
			userRepository.save(u);
		}
    }
	
	public boolean deleteUser(int id) {
		userRepository.delete(this.getUser(id).getUser());
		return true;
	}

	public List<Card> getCardsByUserId(int id) {
        RestTemplate restTemplate = new RestTemplate();
        List<Card> cards = cardService.getCardsByUserId(id);
        return cards;
	}
}