package pokemon.atelier2.MS_rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pokemon.atelier2.MS_controller.MarketService;
import pokemon.atelier2.MS_model.Card;

@RestController
@RequestMapping("/api/market")
public class MarketRestCtrl {

    @Autowired
    MarketService mService;

    @PutMapping("/buy/{idBuyer}/{idCard}")
    public int buyCard(@PathVariable int idBuyer, @PathVariable int idCard) {
        return mService.buyCard(idBuyer, idCard);
    }

    @PutMapping("/sell/{idSeller}/{idCard}")
    public int sellCard(@PathVariable int idSeller, @PathVariable int idCard) {
        return mService.sellCard(idSeller, idCard);
    }

    @GetMapping("/market")
    public List<Card> getMarketCards() {
        return mService.getMarketCards();
    }
}