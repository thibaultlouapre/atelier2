package pokemon.atelier2.MS_repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import pokemon.atelier2.MS_model.CardDescription;

public interface CardDescriptionRepo extends CrudRepository<CardDescription, Integer>{
	
	public List<CardDescription> findAll();
}

