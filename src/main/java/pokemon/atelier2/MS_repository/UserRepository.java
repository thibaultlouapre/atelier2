package pokemon.atelier2.MS_repository;

import org.springframework.data.repository.CrudRepository;

import pokemon.atelier2.MS_model.User;

public interface UserRepository extends CrudRepository<User, Integer>{

}

