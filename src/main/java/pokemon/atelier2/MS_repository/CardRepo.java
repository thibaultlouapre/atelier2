package pokemon.atelier2.MS_repository;

import org.springframework.data.repository.CrudRepository;

import pokemon.atelier2.MS_model.Card;

public interface CardRepo extends CrudRepository<Card, Integer> {
	
}

